# Presentation slides

This repository contains all presentations related to R3.

# Preferred method

<img src="https://webdav-r3lab.uni.lu/public/R3lab/logos/warning.png" height="20px" width="20px"> Install Docker first: https://docs.docker.com/install/

<img src="https://webdav-r3lab.uni.lu/public/R3lab/logos/warning.png" height="20px" width="20px"> Please clone using the following command:
```bash
$ git clone --recurse-submodules ssh://git@git-r3lab-server.uni.lu:8022/R3/outreach/templates/presentation.git
```

Then, run:

```bash
$ ./contribute.sh -d YYYY-MM-DD -n presentationName
```
Your slides are now in `YYYY/YYYY-MM-DD_presentationName/slides`.
The first time, the underlying docker image will be downloaded (pulled). This may take a while.

Once the slides have been compiled, a local server is running under http://localhost:9000. You may need to access this server under http://127.0.0.1:9000 or http://0.0.0.0:9000.

In order to stop the execution, hit `CTRL+C`. Rerun the same command above to continue working on the presentation.

**Example** for a Xmas presentation:
```bash
$ ./contribute.sh -d 2019-12-25 -n xmas
```
The slides are in `2019/2019-12-25_xmas/slides`.

## Adding a new slide/slides

Create a JSON file with your slide(s) in `YYYY-MM-DD_presentationName/slides` and place it into `list.json` file at desired position:
```
[
    { "filename": "index.md" },
    { "filename": "structure.md" },
    { "filename": "myNewAwesomeSlide.md" },
    { "filename": "thanks.md" }
]
```

## How to continue working on a presentation

 For resuming your work on a presentation, you can run the same contribute command:</br>
 ```bash
 $ ./contribute.sh -d YYYY-MM-DD -n presentationName
 ```

## Further information

More information is in the `docs` folder.