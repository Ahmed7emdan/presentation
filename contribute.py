import click
import datetime
import os
from distutils.dir_util import copy_tree
from shutil import copyfile

@click.command()
@click.option('--date', default=datetime.datetime.today().strftime('%Y-%m-%d'), help='Date of the presentation - format: YYYY-MM-DD')
@click.option('--name', default='myPresentation', help='Short name of the presentation.')

def main(date, name):
    """Copies the template folder"""

    # validate the date
    validateDate(date)

    # get the directory
    mainDir = date[:4]

    # get the root directory
    rootDir = os.getcwd()

    # create a root directory if not existing
    if not os.path.exists(mainDir):
        os.mkdir(mainDir)

    # generate the full name of the presentation
    fullName = date + "_" + name

    # generate the full path
    fullPath = os.path.join(os.getcwd(), mainDir, fullName)
    slidesPath = os.path.join(fullPath, 'slides')

    # print out a summary
    click.echo(' > Date: {0}' . format(date))
    click.echo(' > Name: {0}' . format(name))
    click.echo(' > Directory: {0}' . format(fullPath))
    click.echo(' > Root directory: {0}' . format(rootDir))

    # create the directory
    if not os.path.exists(fullPath):
        os.mkdir(fullPath)
        # create an empty slides folder inside
        if not os.path.exists(slidesPath):
            os.mkdir(slidesPath)

        click.echo(' > Directory for slides {0} created.' . format(slidesPath))
    else:
        click.echo(' > Directory for slides{0} already exists.' . format(slidesPath))

    # change to the root directory of the presentation
    os.chdir(fullPath)

    # generate the symlink to the theme
    createSymlink('../../theme', 'theme')
    createSymlink('../../theme/package.json', 'package.json')

    # copy the contents of the template folder
    if not os.path.isfile(os.path.join(fullPath, 'slides', 'index.md')):
        copy_tree(os.path.join(rootDir, 'template', 'slides'), slidesPath)
        click.echo(' > Template slides copied.')
    else:
        click.echo(' > Slide deck already exists.')

    # copy the Gruntfile
    if not os.path.isfile(os.path.join(fullPath, 'Gruntfile.coffee')):
        copyfile(os.path.join(rootDir, 'template', 'Gruntfile.coffee'), os.path.join(fullPath, 'Gruntfile.coffee'))
        click.echo(' > Gruntfile copied.')
    else:
        click.echo(' > Gruntfile already exists.')


    # launch the presentation deck
    if not os.path.exists(os.path.join(fullPath, 'node_modules')):
        click.echo(' > Installing dependencies ... ')
        os.system('yarn add -g grunt-cli generator-reveal')
        os.system('yarn')
        click.echo(' > All dependencies installed.')
    else:
        click.echo(' > All dependencies already installed.')

    # launch the server
    os.system('grunt server')

def createSymlink(src, dst):
    if not os.path.islink(dst):
        os.symlink(src, dst)
        click.echo(' > Symlink to {0} created.' . format(dst))
    else:
        click.echo(' > Symlink to {0} already exists.' . format(dst))


def validateDate(input):
    """Validate a date string to fit the ISO format"""
    try:
        datetime.datetime.strptime(input, '%Y-%m-%d')
    except ValueError:
        print('The date {} is invalid'.format(input))
        raise

if __name__ == '__main__':
    main()
