# Generated on 2013-11-27 using generator-reveal 0.3.2
module.exports = (grunt) ->

    grunt.loadNpmTasks 'grunt-gh-pages'

    grunt.initConfig

        watch:

            livereload:
                options:
                    livereload: true
                files: [
                    'index.html'
                    'slides/*.md'
                    'slides/*.html'
                    'theme/js/*.js'
                ]

            index:
                files: [
                    'theme/templates/_index.html'
                    'theme/templates/_section.html'
                    'slides/list.json'
                ]
                tasks: ['buildIndex']

            jshint:
                files: ['theme/js/*.js']
                tasks: ['jshint']

        connect:

            livereload:
                options:
                    port: 9000
                    # Change hostname to '0.0.0.0' to access
                    # the server from outside.
                    #hostname: '0.0.0.0'
                    base: '.'
                    #open: true
                    livereload: true

        jshint:

            all: ['theme/js/*.js']

        copy:

            dist:
                files: [{
                    expand: true
                    src: [
                        'slides/**'
                        'theme/**'
                        'theme/css/**'
                        'theme/js/**'
                        'slides/img/**'
                    ]
                    dest: 'public/'
                },{
                    expand: true
                    src: ['index.html']
                    dest: 'public/'
                    filter: 'isFile'
                }]


    # Load all grunt tasks.
    require('load-grunt-tasks')(grunt)

    grunt.registerTask 'buildIndex',
        'Build theme/templates/_index.html and slides/list.json.',
        ->
            indexTemplate = grunt.file.read 'theme/templates/_index.html'
            sectionTemplate = grunt.file.read 'theme/templates/_section.html'
            slides = grunt.file.readJSON 'slides/list.json'

            html = grunt.template.process indexTemplate, data:
                slides:
                    slides
                section: (slide) ->
                    grunt.template.process sectionTemplate, data:
                        slide:
                            slide
            grunt.file.write 'index.html', html

    grunt.registerTask 'test',
        '*Lint* javascript and coffee files.', [
            'jshint'
        ]

    grunt.registerTask 'server',
        'Run presentation locally and start watch process (living document).', [
            'buildIndex'
            'connect:livereload'
            'watch'
        ]

    grunt.registerTask 'dist',
        'Save presentation files to *public* directory.', [
            'test'
            'buildIndex'
            'copy'
        ]

    # Define default task.
    grunt.registerTask 'default', [
        'test'
        'server'
    ]
